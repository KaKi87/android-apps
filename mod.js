const axios = require('axios');

module.exports = {
    find: async (
        id,
        {
            source = 'aptoide'
        } = {}
    ) => {
        const result = {
            /** @type {string} */
            icon: undefined,
            /** @type {string} */
            name: undefined,
            /** @type {string} */
            description: undefined,
            /** @type {string[]} */
            permissions: undefined,
            /** @type {string} */
            version: undefined,
            /** @type {number} */
            size: undefined,
            /** @type {string[]} */
            urls: undefined
        };
        switch(source){
            case 'aptoide': {
                const {
                    data
                } = await axios({
                    method: 'POST',
                    url: 'https://webservices.aptoide.com/webservices/3/getApkInfo',
                    data: {
                        'identif': `package:${id}`,
                        'mode': 'json'
                    }
                });
                if(data['status'] === 'OK'){
                    result.icon = data['apk']['icon_hd'];
                    result.name = data['meta']['title'];
                    result.description = data['meta']['description'];
                    result.permissions = data['apk']['permissions'];
                    result.version = data['apk']['vername'];
                    result.size = data['apk']['size'];
                    result.urls = [
                        data['apk']['path'],
                        data['apk']['altpath']
                    ];
                }
                break;
            }
        }
        return result;
    }
};